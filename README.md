# Vector Pinball

Vector Pinball is a pinball game for Android devices.
It is released under version 3 of the GPL; see [COPYING](COPYING.txt) for the license text.

This is an edited version of The GitHub project: [github.com/dozingcat/Vector-Pinball/](https://github.com/dozingcat/Vector-Pinball/).
See [devnotes.txt](devnotes.txt) for an overview of the code layout.
